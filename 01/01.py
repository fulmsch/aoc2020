#!/usr/bin/env python3

lines = open('input', 'r').readlines()

numbers = [int(line) for line in lines]

goal = 2020
sol1 = False
sol2 = False

for a in numbers:
    for b in numbers:
        if not sol1 and a + b == goal:
            sol1 = a*b
        for c in numbers:
            if not sol2 and a + b + c== goal:
                sol2 = a*b*c

print("Solution 1:", sol1)
print("Solution 2:", sol2)
