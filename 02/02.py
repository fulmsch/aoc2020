#!/usr/bin/env python3

lines = open('input', 'r').readlines()

passwords = []
rules = []

i = 0
for l in lines:
    l = l.split('-')
    a = l[1].split(' ')
    passwords.append(a[2][:-1])
    rules.append({'lower':int(l[0]), 'upper':int(a[0]), 'letter':a[1][0]})

valid = 0

for i in range(len(passwords)):
    pw = passwords[i]
    first = rules[i]['lower'] - 1
    second = rules[i]['upper'] - 1
    letter = rules[i]['letter']
    if (pw[first] == letter) != (pw[second] == letter):
        valid = valid + 1

print(valid)
