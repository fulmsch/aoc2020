#!/usr/bin/env python3


def f(lines, x_step, y_step):
    width = len(lines[0])

    x = 0
    y = 0

    trees = 0

    for l in range(len(lines) - 1):
        x = x + x_step
        if x >= width:
            x = x % width
        y = y + y_step
        try:
            if lines[y][x] == '#':
                trees = trees + 1
        except(IndexError):
            pass

    return trees



lines = [l.rstrip() for l in open('input', 'r').readlines()]
print(len(lines))



print("Part 1:", f(lines, 3, 1))
print("Part 2:", f(lines, 1, 1) * f(lines, 3, 1) * f(lines, 5, 1) * f(lines, 7, 1) * f(lines, 1, 2))
