#!/usr/bin/env python3

import re

data = open('input', 'r').read().rstrip()


keys = set(['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'])

passports = [line.replace('\n', ' ').split(' ') for line in data.split('\n\n')]

part1 = 0
part2 = 0

for p in passports:
    fields = set([s[:3] for s in p])
    if keys.issubset(fields):
        part1 = part1 + 1
    else:
        continue

    f = {s[:3]: s[4:] for s in p}

    if len(f['byr']) != 4 or int(f['byr']) < 1920 or int(f['byr']) > 2020:
        continue
    if len(f['iyr']) != 4 or int(f['iyr']) < 2010 or int(f['iyr']) > 2020:
        continue
    if len(f['eyr']) != 4 or int(f['eyr']) < 2020 or int(f['eyr']) > 2030:
        continue

    h = f['hgt']
    if h[-2:] == 'cm':
        if int(h[:-2]) < 150 or int(h[:-2]) > 193:
            continue
    elif h[-2:] == 'in':
        if int(h[:-2]) < 59 or int(h[:-2]) > 76:
            continue
    else:
        continue

    if not re.match(r'^#[0-9a-f]{6}$', f['hcl']):
        continue

    for c in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        if f['ecl'] == c:
            break
    else:
        continue

    if not re.match(r'^\d{9}$', f['pid']):
        continue

    part2 = part2 + 1




print('Part 1:', part1)
print('Part 2:', part2)
