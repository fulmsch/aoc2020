#!/usr/bin/env python3

def get_id(boarding_pass):
    row = int(boarding_pass[0:7].replace('F', '0').replace('B', '1'), 2)
    col = int(boarding_pass[7:].replace('L', '0').replace('R', '1'), 2)
    return row * 8 + col

lines = [l.rstrip() for l in open('input', 'r').readlines()]

ids = [get_id(l) for l in lines]
ids.sort()

print('Part 1:', max(ids))

for i in range(1, len(ids)):
    if ids[i] != 1 + ids[i-1]:
        print('Part 2:', ids[i] - 1)
        break
