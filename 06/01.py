#!/usr/bin/env python3

forms = [s.split('\n') for s in open('input', 'r').read().rstrip().split('\n\n')]

part1 = 0
part2 = 0

for f in forms:
    part1 += len(set(''.join(f)))

    a = set(f[0])

    for i in range(1, len(f)):
        b = a.copy()
        for l in b:
            if l not in f[i]:
                a.remove(l)

    part2 += len(a)

print('Part 1:', part1)
print('Part 2:', part2)
