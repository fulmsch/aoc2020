#!/usr/bin/env python3

import re

def find_parents(tree, child):
    ans = []
    for e in tree:
        for c in e['c']:
            if c == child:
                ans.append(e['p'])
    return ans

def find_containers(rules, bag):
    containers = []

    a = find_parents(rules, bag)
    if len(a) != 0:
        containers += a
        for c in a:
            containers += find_containers(rules, c)

    return set(containers)


part1 = 0
part2 = 0

lines = open('input', 'r').readlines()

rules = []

for l in lines:
    r = re.match(r'^(.*) bags contain (.*)\.$', l)
    p = r.group(1)

    rest = r.group(2).split(',')

    c = []
    for x in rest:
        r = re.match(r'(\d+) (.*) bag', x.strip())
        if r:
            c.append(r.group(2))

    rules.append({'p':p, 'c':c})

part1 = len(find_containers(rules, 'shiny gold'))




print("Part 1:", part1)
print("Part 2:", part2)
