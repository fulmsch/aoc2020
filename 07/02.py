#!/usr/bin/env python3

import re

def find_number(rules, bag):
    ans = 1

    for r in rules:
        if r['p'] == bag:
            if r['c'] == []:
                break

            for c in r['c']:
                ans += c['n'] * find_number(rules, c['b'])
    return ans


lines = open('input', 'r').readlines()

rules = []

for l in lines:
    r = re.match(r'^(.*) bags contain (.*)\.$', l)
    p = r.group(1)

    rest = r.group(2).split(',')

    c = []
    for x in rest:
        r = re.match(r'(\d+) (.*) bag', x.strip())
        if r:
            c.append({'n':int(r.group(1)), 'b':r.group(2)})

    rules.append({'p':p, 'c':c})

part2 = find_number(rules, 'shiny gold') - 1

print("Part 2:", part2)
