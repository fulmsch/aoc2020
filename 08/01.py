#!/usr/bin/env python3

lines = open('input', 'r').readlines()

part1 = 0


code = [{'op': l[:3], 'arg': int(l[4:-1])} for l in lines]

visited = set()

pc = 0
acc = 0

while True:
    if pc in visited:
        part1 = acc
        break

    visited.add(pc)

    i = code[pc]

    if i['op'] == 'acc':
        acc += i['arg']
    elif i['op'] == 'jmp':
        pc += i['arg'] - 1

    pc += 1


print("Part 1:", part1)
