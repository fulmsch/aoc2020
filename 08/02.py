#!/usr/bin/env python3

import copy

lines = open('input', 'r').readlines()

part2 = 0


orig_code = [{'op': l[:3], 'arg': int(l[4:-1])} for l in lines]


for x in range(len(orig_code)):
    code = copy.deepcopy(orig_code)

    if code[x]['op'] == 'jmp':
        code[x]['op'] = 'nop'
    elif code[x]['op'] == 'nop':
        code[x]['op'] = 'jmp'

    visited = set()
    pc = 0
    acc = 0

    while pc not in visited and pc < len(code):
        visited.add(pc)

        i = code[pc]

        if i['op'] == 'acc':
            acc += i['arg']
        elif i['op'] == 'jmp':
            pc += i['arg'] - 1
        elif i['op'] == 'nop':
            pass

        pc += 1

    if pc == len(code):
        part2 = acc
        break


print("Part 2:", part2)
